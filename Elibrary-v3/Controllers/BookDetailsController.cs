﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Elibrary.Models;
using Elibrary_v3.Data;

namespace Elibrary_v3.Controllers
{
    public class BookDetailsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BookDetailsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: BookDetails
        public async Task<IActionResult> Index()
        {
            return View(await _context.BookDetails.ToListAsync());
        }

        // GET: BookDetails/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookDetailsModel = await _context.BookDetails
                .FirstOrDefaultAsync(m => m.BookDetailsId == id);
            if (bookDetailsModel == null)
            {
                return NotFound();
            }

            return View(bookDetailsModel);
        }

        // GET: BookDetails/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: BookDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("BookDetailsId,Title,Autor,Gender,Description")] BookDetailsModel bookDetailsModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(bookDetailsModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(bookDetailsModel);
        }

        // GET: BookDetails/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookDetailsModel = await _context.BookDetails.FindAsync(id);
            if (bookDetailsModel == null)
            {
                return NotFound();
            }
            return View(bookDetailsModel);
        }

        // POST: BookDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("BookDetailsId,Title,Autor,Gender,Description")] BookDetailsModel bookDetailsModel)
        {
            if (id != bookDetailsModel.BookDetailsId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(bookDetailsModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookDetailsModelExists(bookDetailsModel.BookDetailsId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(bookDetailsModel);
        }

        // GET: BookDetails/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookDetailsModel = await _context.BookDetails
                .FirstOrDefaultAsync(m => m.BookDetailsId == id);
            if (bookDetailsModel == null)
            {
                return NotFound();
            }

            return View(bookDetailsModel);
        }

        // POST: BookDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var bookDetailsModel = await _context.BookDetails.FindAsync(id);
            _context.BookDetails.Remove(bookDetailsModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookDetailsModelExists(int id)
        {
            return _context.BookDetails.Any(e => e.BookDetailsId == id);
        }
    }
}
