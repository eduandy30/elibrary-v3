﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Elibrary.Models;
using Elibrary_v3.Data;
using System.IO;
using Microsoft.AspNetCore.Http;
using NAudio.Wave;

namespace Elibrary_v3.Controllers
{
    public class BooksController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BooksController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Books
        public async Task<IActionResult> Index()
        {
            var book = (from Book
                       in _context.Books
                        join Details in _context.BookDetails on Book.BookDetailsId
                        equals Details.BookDetailsId
                        where Book.BookDetailsId == Details.BookDetailsId
                        select new BooksViewModel() { Books = Book , BookDetails = Details });

            return View(book);
        }

        // GET: Books/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booksModel = (from Book
                       in _context.Books
                              join Details in _context.BookDetails on Book.BookDetailsId
                              equals Details.BookDetailsId
                              where Book.BookDetailsId == Details.BookDetailsId
                              select new BooksViewModel() { Books = Book, BookDetails = Details}).FirstOrDefault(x => x.Books.BookId == id);
            
            // await  Download(booksModel.Result.Books.Books);

            if (booksModel == null)
            {
                return NotFound();
            }

            return View(booksModel);
        }

        // GET: Books/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Books/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(BooksModel booksModel)
        {
            if (ModelState.IsValid)
            {
                if (booksModel.File == null || booksModel.File.Length == 0)
                    return Content("file not selected");

                var path = Path.Combine(Directory.GetCurrentDirectory(), "Books", booksModel.File.FileName);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await booksModel.File.CopyToAsync(stream);
                }
                booksModel.Books = booksModel.File.FileName;
                _context.Add(booksModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(booksModel);
        }
       
        public async Task<IActionResult> Download(string fileName, string folder = "Books")
        {
            if (fileName == null)
                return Content("filename not present");

            var path = Path.Combine(
                           Directory.GetCurrentDirectory(),
                           folder, fileName);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, GetContentType(path), Path.GetFileName(path));
        }
        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet"},  
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"},
                {".mp3", "audio/mpeg" },
                {".mp4", "audio/mp4" }
            };
        }

        // GET: Books/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booksModel = await _context.Books.FindAsync(id);
            if (booksModel == null)
            {
                return NotFound();
            }
            return View(booksModel);
        }

        // POST: Books/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, BooksModel booksModel)
        {
            if (id != booksModel.BookId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (booksModel.File == null || booksModel.File.Length == 0)
                        return Content("file not selected");

                    var path = Path.Combine(Directory.GetCurrentDirectory(), "Books", booksModel.File.FileName);

                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await booksModel.File.CopyToAsync(stream);
                    }
                    booksModel.Books = path;
                    _context.Update(booksModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BooksModelExists(booksModel.BookId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(booksModel);
        }

        // GET: Books/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booksModel = await _context.Books
                .FirstOrDefaultAsync(m => m.BookId == id);
            if (booksModel == null)
            {
                return NotFound();
            }

            return View(booksModel);
        }

        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var booksModel = await _context.Books.FindAsync(id);
            _context.Books.Remove(booksModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BooksModelExists(int id)
        {
            return _context.Books.Any(e => e.BookId == id);
        }
    }
}
