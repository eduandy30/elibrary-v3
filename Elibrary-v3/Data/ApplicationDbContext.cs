﻿using System;
using System.Collections.Generic;
using System.Text;
using Elibrary.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Elibrary_v3.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }
        public DbSet<BooksModel> Books { get; set; }
        public DbSet<BookDetailsModel> BookDetails { get; set; }
    }
}
