﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Elibrary.Models
{
    public class BookDetailsModel
    {
        [Key]
        public int BookDetailsId { get; set; }
        public string Title { get; set; }
        public string Autor { get; set; }
        public string Gender { get; set; }
        public string Description { get; set;}
    }
}
