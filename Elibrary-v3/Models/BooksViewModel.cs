﻿using Elibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elibrary.Models
{
    public class BooksViewModel
    {
        public  BooksModel Books { get; set; }
        public BookDetailsModel BookDetails { get; set; }
    }
}
