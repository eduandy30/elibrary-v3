﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Elibrary.Models
{
    public class BooksModel
    {
        [Key]
        public int BookId { get; set; }

        [DataType(DataType.Text)]
        public string Books { get; set;}
        public int BookDetailsId { get; set; }
        public BookDetailsModel BookDetails { get; set; }
        [NotMapped]
        public IFormFile File { get; set; } 

    }
}
